import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class StockGrab {

	public static void main(String[] args) throws FileNotFoundException {
		//File to store the stock data
		File file = new File("C:\\Users\\CambierRyan\\Documents\\StockData\\inputData.csv");
		PrintWriter out = new PrintWriter(file);
		URL url;
		try {
			//URL to collect the stock data from, the parameters are the following:
			//s = stock ticker, a = start month, b = start day, c = start year
			//d = end month, e = end day, f = end year, g = interval (d = day, w = week, m = month)
			//A string is made for each time period to be evaluated
			String a = "http://ichart.finance.yahoo.com/table.csv?s=yhoo&a=0&b=01&c=2005&d=2&e=30&f=2005&g=d&ignore=.csv";
			//The exception for Baidu's stock
			//String a = "http://ichart.finance.yahoo.com/table.csv?s=bidu&a=0&b=01&c=2006&d=2&e=30&f=2006&g=d&ignore=.csv"; 
			//String a = "http://ichart.finance.yahoo.com/table.csv?s=yhoo&a=0&b=01&c=2007&d=5&e=30&f=2007&g=d&ignore=.csv";
			//String a = "http://ichart.finance.yahoo.com/table.csv?s=yhoo&a=0&b=01&c=2010&d=9&e=31&f=2010&g=d&ignore=.csv";
			//String a = "http://ichart.finance.yahoo.com/table.csv?s=bidu&a=0&b=01&c=2011&d=9&e=31&f=2011&g=d&ignore=.csv";
			//String a = "http://ichart.finance.yahoo.com/table.csv?s=yhoo&a=8&b=01&c=2013&d=11&e=31&f=2013&g=d&ignore=.csv";

			url = new URL(a);
			URLConnection conn = url.openConnection();
			// open the stream and put it into BufferedReader
			BufferedReader br = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			
		    ArrayList<String> list = new ArrayList<String>(); //List to hold the data
			String inputLine; //Current line being evaluated
			String cvsSplitBy = ",";
			//Queue for the moving weighted average classification
			Queue <Double> diffs = new ArrayBlockingQueue <Double> (6);
			while ((inputLine = br.readLine()) != null) {
				// use comma as separator
				String[] stocknums = inputLine.split(cvsSplitBy);
				// Get the stock values
				String sopen = stocknums[1];
				String sclose = stocknums[4];
				//Convert to double
				double dopen = makeDouble(sopen);
				double dclose = makeDouble(sclose);	
				//Find the difference, used for the first 4 lines of data
				double openclosediff = dclose - dopen;
				// Label is 1 = loss and 2 = gain, based on the moving weighted average 
				int label = 0;
				diffs.add(openclosediff); //Add the current difference value to the queue
				//Make sure the size of the queue is at most 5 
				//so the MWA formula can be applied
				if (diffs.size() > 5) {
					diffs.remove();
				}
				//Check if the MWA formula can be applied (when size reaches 5)
				if (diffs.size() == 5) {
					//Apply the MWA formula and return the label
					label = mwaFind(diffs);
				}				
				//Initializes the first four elements based on a simple
				//formula, if the difference is greater than 0, label as gain
				//if the difference is less, label as loss, neutral if their is
				//no difference
				if (diffs.size() < 5){
				if (openclosediff > 0) {
					label = 2;
				} else if (openclosediff < 0) {
					label = 1;
				} else if (openclosediff == 0) {
					label = 0;
				} 
				}
				// Make the integer label a string so it can be appended to the data
				String stringlabel = Double.toString(label);
				//Add the line of data and the label given 
				list.add(inputLine + "," + stringlabel);				
			} // closes the while loop
			//Save the data label so it stays at the top of the file
			String datalabels = list.get(0);
			//Temporarily remove the labels so the data can be reversed
			list.remove(0);
			//Puts the data in correct order, earlier date -> later date
			Collections.reverse(list);
			//Add the labels to the file first
			out.println(datalabels);			
			//Add the data to the file
			for (int i = 0; i < list.size(); i++)
			{
				String s = list.get(i).toString();
				//removes the brackets on both sides
				s.substring(1, s.length()-1);
				//adds the current line of data to the file
				out.println(s);
			}	
			out.close(); //close the printwriter
			br.close(); //close the bufferedreader
			System.out.println("Done");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(
					"The data is invalid, type double is required.", nfe);
		}
	}
	//Original parseDouble wasn't working, this works just as well
	public static double makeDouble(String str) {
		try {
			double newnum = Double.parseDouble(str);
			return newnum;
		} catch (NumberFormatException e) {
			return 00;
		}
	}
	//Method to compute the moving weighted average for a line of data
	//This take the current difference in open and close price, as well
	//as the differences from the previous four days, and adds them up
	//with a weight assigned to each difference.
	public static int mwaFind (Queue <Double> diffs) {
		int label = 0; //Label for the data, 1 = Loss, 2 = Gain
		//Create array to hold the values being used for the 
		//MWA formula
		Double [] diffarray = diffs.toArray(new Double[5]);
		//Weights are 1/15, 2/15, 3/15, 4/15, 5/15
		//Rounded down to 10 decimal places
		double weight1 = 0.0666666667; //4th previous day weight
		double weight2 = 0.1333333333; //3rd previous day weight
		double weight3 = 0.2000000000; //2nd previous day weight
		double weight4 = 0.2666666667; //Previous Day weight
		double weight5 = 0.3333333333; //Current Day weight
		
		//The 5 current elements being evaluated
		//Includes the current day's difference and the 
		//previous 4 days differences
		//Since the queue was converted to this array,
		//the most recently added element (diffarray[0])
		//is the latest
		double diff1 = diffarray[0]; //4th previous day difference
		double diff2 = diffarray[1]; //3rd previous day difference
		double diff3 = diffarray[2]; //2nd previous day difference
		double diff4 = diffarray[3]; //Previous Day difference
		double diff5 = diffarray[4]; //Current day difference
		//Calculate the MWA
		double finalweight = (weight1 * diff1)
				+ (weight2 * diff2) + (weight3 * diff3)
				+ (weight4 * diff4) + (weight5 * diff5);
		//Assign the labels based on the MWA result
		if (finalweight > 0) {
			label = 2;
		}
		else if (finalweight < 0){
			label = 1;
		}
		else if (finalweight == 0){
			label = 0;
		}
		return label;
	}
}
