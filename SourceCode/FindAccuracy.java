import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FindAccuracy {

	public static void main(String[] args) {
		String csvSplitBy = ",";
		//Original data file
		File actualfile = new File("C:\\Users\\CambierRyan\\Documents\\StockData\\inputData.csv");
		//File containing predictions made by the GFS
		File predictfile = new File("C:\\Users\\CambierRyan\\Documents\\StockData\\Scripts\\predictions\\outputPredictions.csv");
		//Holds the original classification values
		double [] dactuals = new double[findLength(actualfile)];
		//Holds the predicted classification values
		double [] dpredicts = new double[findLength(actualfile)];
		//Try-catch to grab all the original classification values
		try {
			FileReader fileReader1 = new FileReader(actualfile);
			BufferedReader bufferedReader = new BufferedReader(fileReader1);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			bufferedReader.readLine(); //Skip the first line of labels
			int i = 0; //Counter to make sure the array size matches the number of lines of data
			while ((line = bufferedReader.readLine()) != null) {
				String[] stocknums = line.split(csvSplitBy);
				//The 7th position contains the classifications
				String classification = stocknums[7];
				double dclass = Double.parseDouble(classification);
				//Add to the array
				dactuals[i] = dclass;
				i++; //Iterate the array to the next position
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			fileReader1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Try-catch to grab all the predicted classification values
		try {
			FileReader fileReader2 = new FileReader(predictfile);
			BufferedReader bufferedReader = new BufferedReader(fileReader2);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			bufferedReader.readLine(); //Skip the first line of labels
			int x = 0; //Counter for the array of predictions (double values)
			while ((line = bufferedReader.readLine()) != null) {
				//Replaces all the spaces between values with a single space
				String theline = line.trim().replaceAll(" +", " ");
				//Splits the string by the single space
				String[] splitline = (theline.split("\\s"));
				double prediction = Double.parseDouble(splitline[1]);
				//Add the prediction value to the array
				dpredicts[x] = prediction;
				x++; //Iterate the array to the next position
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			fileReader2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		double onenum = 1.0; //Fixed value to measure the accuracy
		//Find the error value between all the predictions and original values
		double error = findError(dactuals, dpredicts);
		//Calculate the accuracy, by percentage
		double accuracy = (onenum - error) * 100;
		System.out.println("Average classification accuracy is: ");
		System.out.printf("%.4f", accuracy);
	}
	//Finds the length of a data file
	public static int findLength (File input) {
		int i = 0; //Counts the number of lines
		try {
		FileReader fileReader = new FileReader(input);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			i++; //Add to the counter for each line that is read
		}
		fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return i;
	}
		
	//Finds the error between all the predicted and actual values
	public static double findError (double[] actuals, double[] predicts){
		double sum = 0; //Total error amount
		double actlength = (double) actuals.length;
		//Loop until the second to last line
		for (int i=0; i < actlength-1; i++){
			//Absolute value applied to the difference
			sum = sum + Math.abs(predicts[i] - actuals[i]);
		}
		//Divide the sum by the population size
		double result = (sum / actlength);	
		return result;
	}	
	}
