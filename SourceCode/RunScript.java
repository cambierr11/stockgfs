import java.io.IOException;

public class RunScript {

	public static void main(String[] args) throws IOException, InterruptedException {
		//The start time for running the R script
		double start = System.nanoTime();
		//Runs a BATCH file that executes an R script, containing the fugeR
		//commands to create the GFS and create the predictions
		Process p = Runtime.getRuntime().exec("cmd /c start /wait C:/Users/CambierRyan/Documents/StockData/Scripts/runRscript.bat");
		p.waitFor(); //Wait for the process to finish, so it can be timed properly
		//End time for running the R script
		double end = System.nanoTime();
		double diff = end - start; //Time to run the script
		double secondsdiff = (diff * 0.000000001); //Convert  to seconds
		System.out.println("Time to create model : ");
		System.out.printf("%.4f", secondsdiff); //Format to 4 decimal places

	}

}
