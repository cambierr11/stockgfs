# README #

### What is this repository for? ###

Contains the source code for my 2014-2015 senior project at Allegheny College, "Stock Forecasting using Genetic Fuzzy Systems". In addition, a BATCH file (runRscript.bat) and R script (CreateModel.R) are included, which the system requires.

Also contains the input/output data and implementation results of the experiments.

### How do I get set up? ###

This is ran on a system with Windows 7, so some of the dependencies or 
DEPENDENCIES:
Java SE Development Kit
R programming environment (to run the script)
The following R packages installed: snow, snowfall, Rcpp

___________________________________________________________________________


The system collects the data (StockGrab.java), runs the R script that creates the GFS and outputs predictions (RunScript.java), and finally finds the accuracy of the predictions (FindAccuracy.java).

___________________________________________________________________________

Make the following changes to the code in order to run the system:

"StockGrab.java" : Line 19, change to fit your own file path.

"StockGrab.java" :  (Optional) Lines 27 - 33, change the query parameters to get more or less stock quotes for data or the interval (daily, weekly, monthly):
a = start day
b = start month
c = start year
d = end day
e = end month
f = end year
g = interval



"RunScript.java" : Line 10, change the process to match the file location of your BATCH file, if you are using Windows.

In Ubuntu/Linux, you can simply replace the process with "Rscript createModel.R", assuming you have R installed and you are pointing to the directory containing the script.


"FindAccuracy.java" : Line 11, change the location of variable "actualfile" to match the location of your data collected from "StockGrab.java".

"FindAccuracy.java" : Line 13, change the location of variable "predictfile" to match the location of your output predictions that is set in the R script (Ex: sink("predictions/outputPredictions.csv")).

___________________________________________________________________________


The final changes require modification of the R script and the BATCH file.

For the BATCH file:
Change the "cd" commands to match the location of your "CreateModel.R"
Set the R_Script variable to match the location of your "Rscript.exe" file.


For the R script (assuming you have the required packages installed):
Line 1, Change the command to set the directory of your R environment, the directory you set should contain your data file.

Line 3, Set the first "sink" command to an empty text file that you create, so you can collect the verbose R output.

Line 4, Set the "pdf" command to a location determined by you, to store the output graph of predicted vs actual values.

Line 5, Set the "read.csv" command to your CSV data file that you collect with "StockGrab.java".

(Optional) Line 10, Alter the parameters of the command to create different fuzzy systems with a modified genetic algorithm.

Line 14, Change the second "sink" command to store the systems set of predictions in a CSV file created by you.


### Contribution guidelines ###

This project was created under the guidance of Professor Gregory Kapfhammer and Professor Janyl Jumadinova.


### Who do I talk to? ###

Ryan Cambier
<rcambier11@gmail.com>